﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public bool isCameraRotationInverted = true;
    public float cameraPlayerDelay = 3f;

    private Transform player;
    private PlayerController playerController;
    private float cameraDiff;

    void Start()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        player = playerObject.transform;
        playerController = playerObject.GetComponent<PlayerController>();
        cameraDiff = Mathf.Abs(player.position.z - transform.position.z);
    }

    void Update()
    {
        float rotDirection = isCameraRotationInverted ? (-1) : 1;
        float directionX = playerController.direction.x * cameraPlayerDelay;
        float directionZ = playerController.direction.z * cameraPlayerDelay;
        transform.position = new Vector3(
			player.position.x + cameraDiff * Mathf.Sin(toRadians(rotDirection * player.eulerAngles.y)) + directionX ,
            transform.position.y,
			player.position.z + cameraDiff * Mathf.Cos(toRadians(rotDirection * player.eulerAngles.y)) + directionZ
            );
    }

	float toRadians(float deg){
		return (deg*Mathf.PI)/180;
	}
}
