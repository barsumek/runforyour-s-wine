﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RunIntro : MonoBehaviour {

    public Sprite[] images;
    public float[] times;
    private int index = 0;
    public bool isOutro = false;

    private Image currentImage;
    void Start()
    {
       currentImage = GetComponent<Image>();
        if(!AudioManager.instance.introPlayed&&!isOutro)
        {
            StartCoroutine(Run());
        }
        else if (isOutro)
        {
            StartCoroutine(Run());
        }
        else
            gameObject.SetActive(false);

    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            StopCoroutine(Run());
            AudioManager.instance.introPlayed = true;
            gameObject.SetActive(false);
        }
        if(Input.GetKeyDown(KeyCode.Space)&&isOutro)
        {
            StopCoroutine(Run());
            Application.LoadLevel(0);
        }
    }
    
        
    
    IEnumerator Run()
    {
        currentImage.sprite = images[index];
        index += 1;
        yield return new WaitForSeconds(times[index-1]);
        if (index < 5)
            StartCoroutine(Run());
        else if (isOutro)
        {
            Application.LoadLevel(0);
        }
        else
        {
            gameObject.SetActive(false);
            AudioManager.instance.introPlayed = true;
        }
    }
}
