﻿using UnityEngine;
using System.Collections;

public class PlayerInAZS : MonoBehaviour {
	public Animator azsAnim;
	
	void OnTriggerEnter(Collider collide){
		if(collide.tag == "Player")
			azsAnim.SetBool("attack", true);
	}
	
	void OnTriggerExit(Collider collide){
		if(collide.tag == "Player")
			azsAnim.SetBool("attack", false);
	}
}
