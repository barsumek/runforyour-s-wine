﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
    [HideInInspector]
    public static GameManager instance=null;
    void Awake()
    {
        //Managing calling from other scripts
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

       
              
    }
    /// <summary>
    /// Restarts the level
    /// </summary>
    void RestartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    void GameOver()
    {
        //TODO 
    }
    public void QuitGame()
    {
        PlayerPrefs.DeleteKey("mute");
        Application.Quit();
    }
    public void StartGame()
    {
        AudioManager.instance.StopClip();
        Application.LoadLevel(1);
    }

}
