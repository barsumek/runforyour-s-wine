﻿using UnityEngine;
using System.Collections;

public class Train : MonoBehaviour {

	public float trainSpeed = 2;

	private bool isMoving = false;

	void FixedUpdate () {
		if(isMoving)
			transform.position = (transform.position + Vector3.right * trainSpeed * Time.fixedDeltaTime);
	}

	public void activateMove(){
		isMoving = true;
	}
	
}
