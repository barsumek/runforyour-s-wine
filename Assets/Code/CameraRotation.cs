﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {
    public float rotation = 90f;
	public float speed = 500f;
    public bool invertedCameraRotation = true;
    public Vector3 newDirection = new Vector3(0, 0, -1);

	private Transform mainCamera;
	private Transform player;
	private PlayerController playerController;
	
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
		playerController = player.GetComponent<PlayerController>();
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
            //changing player direction from x to z axis
            playerController.direction = newDirection; 
			StartCoroutine(RotatePlayer(rotation));
		}
	}

	IEnumerator RotatePlayer(float rotation)
	{
		var curPlayerAngle = player.transform.eulerAngles;
		var curCameraAngle = mainCamera.transform.eulerAngles;

        mainCamera.GetComponent<CameraController>().isCameraRotationInverted = invertedCameraRotation;


        //Casting to int in order to achieve appropriate angle value
        var newPlayerAngle = (int)curPlayerAngle.y + rotation;
		var newCameraAngle = (int)curCameraAngle.y + (invertedCameraRotation ? (-1) : 1) * rotation;

		while (curPlayerAngle.y != newPlayerAngle){
			// move a little step at constant speed to the new angle:
			curPlayerAngle.y = Mathf.MoveTowards(curPlayerAngle.y, newPlayerAngle, speed*Time.deltaTime);  
			curCameraAngle.y = Mathf.MoveTowards(curCameraAngle.y, newCameraAngle, speed*Time.deltaTime);
			// update the objects rotation
			player.eulerAngles = curPlayerAngle;
			mainCamera.eulerAngles = curCameraAngle;
			yield return null; // and let Unity free till the next frame
		}
	}
}
