﻿using UnityEngine;
using System.Collections;

public class Bottle : MonoBehaviour
{
		
	public float destroyDelay;
    public float flyAfterCollisionPower = 300f;
	private float step;

    private Rigidbody rb;

	void Start ()
    {
		Invoke("WaitAndDestroy", destroyDelay);
		step = Random.Range(360, 720);
        rb = GetComponent<Rigidbody>();
	}

	void Update()
    {
		transform.Rotate(new Vector3(0, 0, -1), step * Time.deltaTime);
	}

	void WaitAndDestroy()
    {
		Destroy(gameObject);
	}

    void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        if (other.tag == "Player" && other.GetComponent<PlayerController>().isRunning)
        {
            rb.AddForce(Vector3.up * flyAfterCollisionPower, ForceMode.Acceleration);
        }
    }
}
