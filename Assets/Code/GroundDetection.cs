﻿using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    private PlayerController playerController;
    private Animator playerAnim;

    void Awake()
    {
        playerController = GetComponentInParent<PlayerController>();
        playerAnim = GetComponentInParent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            playerController.isGrounded = true;
            playerAnim.SetBool("jump", false);
        }
    }

    void OnTriggerStay(Collider other)
    {
        playerController.isGrounded = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            playerController.isGrounded = false;
        }
    }
}
