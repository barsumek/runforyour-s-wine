﻿using UnityEngine;
using System.Collections;

public class ChangeObjectSpeed : MonoBehaviour {
	public float desiredPlayerSpeed;
	public float changeTime;

	private float k; //The speed of changing the speed xD
	private PlayerController playerObject;
	
	IEnumerator changeSpeed(){
		var curPlayerSpeed = playerObject.speed;
		while(curPlayerSpeed != desiredPlayerSpeed){
			curPlayerSpeed = Mathf.MoveTowards(curPlayerSpeed, desiredPlayerSpeed, k*Time.deltaTime);
			playerObject.speed = curPlayerSpeed;
			yield return null;
		}
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
			playerObject = collider.gameObject.GetComponent<PlayerController>();
			k = Mathf.Abs(desiredPlayerSpeed - playerObject.speed)/changeTime;
			StartCoroutine(changeSpeed());
		}
	}
}
