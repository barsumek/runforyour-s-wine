﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
    //Here we can access our sound player
    public static AudioManager instance = null;
    public AudioClip[] clips;
    [HideInInspector]
    public bool introPlayed = false;

    [HideInInspector]
    public bool isMusicOn = true;

    public AudioSource source;

    void Awake()
    {
        //Managing calling from other scripts
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        source = GetComponent<AudioSource>();

    }
    public void PlayClip(AudioClip clip,bool Loop)
    {
        source.loop = Loop;
        source.clip = clip;
        source.Play();
    }
    public void StopClip()
    {
        source.Stop();
    }
    /// <summary>
    /// Function which disables playing sounds
    /// </summary>
    public void SwitchSoundOff(bool mute)
    {
        if (mute)
        {
            source.volume = 0;
        }
        else
        {
            source.volume = 1;
        }
        isMusicOn = !mute;
    }



}
