﻿using UnityEngine;
using System.Collections;

public class ThrowBottles : MonoBehaviour {

	public GameObject [] throwableObjects;
	public float throwCooldown;
	public float startCooldown;
	public Vector3 throwForce;
	public Vector3 positionDelta;
	public Animator enemyAnimator;

	private bool isPlayerIn;
	private int [] probabilityIndexes = {0,0,1,0,1,1,1,1,0,0};


	void ThrowObject(){
		if(throwableObjects.Length == 0) return;
		GameObject justThrowed = Instantiate(throwableObjects[probabilityIndexes[Random.Range(0, probabilityIndexes.Length)]], transform.position + positionDelta, transform.rotation) as GameObject;
		justThrowed.GetComponent<Rigidbody>().AddForce(throwForce, ForceMode.Acceleration);
		justThrowed.transform.parent = transform;
	}

	public void StartThrowing(){
        enemyAnimator.SetBool("attack", true);
        InvokeRepeating("ThrowObject", startCooldown, throwCooldown);
	}

	public void StopThrowing(){
		CancelInvoke("ThrowObject");
		enemyAnimator.SetBool("attack", false);
	}

}

