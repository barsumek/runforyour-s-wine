﻿using UnityEngine;
using System.Collections;

public class ActivateTrain : MonoBehaviour {

	private Train train;

	void Start () {
		train = GetComponentInParent<Train>();
	}


	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
			train.activateMove();
		}
	}
}
