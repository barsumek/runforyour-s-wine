﻿using UnityEngine;
using UnityEngine.UI;

public class FinishGame : MonoBehaviour
{
    public GameObject outro;

    void OnTriggerEnter(Collider other)
    {
        AudioManager.instance.PlayClip(AudioManager.instance.clips[0], false);
        outro.SetActive(true);
    }
}
