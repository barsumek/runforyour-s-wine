﻿using UnityEngine;
using System.Collections;

public class PlayerInRange : MonoBehaviour {

	private ThrowBottles parentEnemy;

    void OnTriggerEnter(Collider other)
	{
        if (other.tag == "Player")
        {
            parentEnemy = GetComponentInParent<ThrowBottles>();
            parentEnemy.StartThrowing();
        }
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            parentEnemy.StopThrowing();
        }
    }
}
