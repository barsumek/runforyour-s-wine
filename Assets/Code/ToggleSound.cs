﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleSound : MonoBehaviour {
    public Toggle t;
    void Start()
    {
        if (PlayerPrefs.HasKey("mute"))
        {
            if (PlayerPrefs.GetInt("mute") == 1)
                t.isOn = true;
            else
                t.isOn = false;
        }
        else
        {
            PlayerPrefs.SetInt("mute", 0);
            t.isOn = false;
        }
                
            
    }

    public void SwitchSoundOff(bool mute)
    {
        if (mute)
        {
            AudioManager.instance.source.volume = 0;
            PlayerPrefs.SetInt("mute", 1);
        }
        else
        {
            AudioManager.instance.source.volume = 1;
            PlayerPrefs.SetInt("mute", 0);
        }
    }
}
