﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    public int jumpPower = 40;
    public float speed = 0.3f;
    public float slideTime = 1.5f;
    public float slideAgainTime = 0.4f;
    public float getUpTime = 0.35f;
    public float attackTime = 0.3f;
    public string filename = "positions.txt";
    [HideInInspector]
    public bool isRunning = true;
    //used by GroundCheck script to change player's state
    [HideInInspector]
    public bool isGrounded = true;

    [HideInInspector]
    public Vector3 direction = new Vector3(-1, 0, 0);

    private bool isSliding = false;
    private bool isAttacking = false;
    private bool canSlide = true;
    private bool isGettingUp = false;

    //used to change player's collider
    private Vector3 normalSize = new Vector3(0.7f, 2f, 0.6f);
    private Vector3 slidingSize = new Vector3(2f, 0.5f, 0.6f);
    private Vector3 normalCenter = new Vector3(0f, 0.5f, 0f);
    private Vector3 slidingCenter = new Vector3(0f, -0.3f, 0f);


    //Player's weapon
    //private GameObject keyboard;

    private Animator anim;
    private Rigidbody rb;
    private BoxCollider col;

    void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.A) && isSliding)
        {
            ChangeSizeAfterSliding();
        }
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded && !isSliding && isRunning && !isGettingUp)
        {
            isGrounded = false;
            anim.SetBool("jump", true);
            rb.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
        }
        else if (Input.GetKey(KeyCode.A) && isGrounded && !isSliding && canSlide)
        {
            col.size = slidingSize;
            col.center = slidingCenter;
            isSliding = true;
            canSlide = false;
            anim.SetBool("slide", isSliding);
            Invoke("ChangeSizeAfterSliding", slideTime);
        }
        else if (Input.GetKeyDown(KeyCode.D) && isGrounded && !isAttacking && (isGettingUp || !isSliding))
        {
            isAttacking = true;
            Attack();
			Invoke("PlayerCanAttack", attackTime);
        }
       
		if (!isRunning && Input.GetKeyDown(KeyCode.R))
        {
			Application.LoadLevel(Application.loadedLevel);
		}

       if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(0);
        }
    }

    void FixedUpdate()
    {
        if (isRunning)
        {
            rb.MovePosition(transform.position + direction * speed * Time.deltaTime);
        }
    }

    private void ChangeSizeAfterSliding()
    {
        if (isSliding && !isGettingUp && !canSlide)
        {
            isGettingUp = true;
            Invoke("SlideEnd", getUpTime);
            anim.SetBool("slide", false);
            Invoke("SlideAgain", slideAgainTime);
        }
    }

    private void SlideEnd()
    {
        col.size = normalSize;
        col.center = normalCenter;
        isSliding = false;
    }

    private void SlideAgain()
    {
        canSlide = true;
        isGettingUp = false;
    }

    /// <summary>
    /// Managing Player's attack
    /// </summary>
    private void Attack()
    {
        anim.SetTrigger("hit");
    }

    private void PlayerCanAttack()
    {
        isAttacking = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MeleeTrigger")
        {
            if (isAttacking)
            {
                other.gameObject.GetComponent<Enemy>().HitByPlayer();
            }
            else
            {
                Die();
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Die();
        }
    }

    private void Die()
    {
        if (isRunning)
        {
            anim.SetTrigger("die");
            GameObject.FindGameObjectWithTag("GameOver").GetComponent<Animator>().SetTrigger("end");
            isRunning = false;
        }
    }

}
