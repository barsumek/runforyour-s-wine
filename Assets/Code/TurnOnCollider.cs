﻿using UnityEngine;

public class TurnOnCollider : MonoBehaviour
{
    public Collider col;
    public GameObject[] turnOnGameObjects;

    void OnTriggerEnter(Collider other)
    {
        col.enabled = true;
        foreach (GameObject go in turnOnGameObjects)
        {
            go.SetActive(true);
        }
    }
}
