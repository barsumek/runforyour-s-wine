﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public float power = 7f;

    private Transform mainCamera;
    private Transform player;
    private Collider enemyCollider;
    private Vector3 inYourFace;

    void Start()
    {
        mainCamera = GameObject.FindWithTag("MainCamera").transform;
        player = GameObject.FindWithTag("Player").transform;
        enemyCollider = GetComponentInParent<BoxCollider>();
    }

    public void HitByPlayer()
    {
        enemyCollider.enabled = false;
        float x = mainCamera.position.x - player.position.x;
        float z = mainCamera.position.z - player.position.z;
        if (Mathf.Abs(x) > Mathf.Abs(z))
        {
            inYourFace = new Vector3(x, 0, 0);
        }
        else
        {
            inYourFace = new Vector3(0, 0, z);
        }
        GetComponentInParent<Rigidbody>().AddForce(inYourFace * power, ForceMode.VelocityChange);
        Destroy(gameObject, 3f);
    }
}