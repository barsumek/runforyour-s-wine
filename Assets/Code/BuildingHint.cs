﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingHint : MonoBehaviour {

    public Text textHint;
    public string buildingDescription;
    private Animator anim;

    void Awake()
    {
        anim = GameObject.FindGameObjectWithTag("BuildingHints").GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            textHint.text = buildingDescription;
            anim.SetTrigger("show");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            anim.SetTrigger("hide");
        }
    }
}
